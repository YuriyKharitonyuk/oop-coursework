﻿using System.IO;

namespace ExplorerDesktop;

internal sealed class ToggleAttributeVisitor : IEntryVisitor
{
    private readonly FileAttributes _attribute;

    public ToggleAttributeVisitor(FileAttributes attribute)
    {
        _attribute = attribute;
    }

    public void Visit(FileModel file)
    {
        Toggle(file);
    }

    public void Visit(DirectoryModel directory)
{
        Toggle(directory);
    }

    private void Toggle(EntryModel entry)
    {
        var info = new FileInfo(entry.Path);

        if (info.Attributes.HasFlag(_attribute))
        {
            info.Attributes &= ~_attribute;
        }
        else
        {
            info.Attributes |= _attribute;
        }
    }
}