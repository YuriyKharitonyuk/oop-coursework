﻿using System.IO;

namespace ExplorerDesktop;

internal sealed class DeleteVisitor : IEntryVisitor
{
    public void Visit(FileModel file)
    {
        File.Delete(file.Path);
    }

    public void Visit(DirectoryModel directory)
    {
        Directory.Delete(directory.Path, true);
    }
}