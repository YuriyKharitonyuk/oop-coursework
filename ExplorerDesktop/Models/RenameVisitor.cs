﻿using System.IO;

namespace ExplorerDesktop;

internal sealed class RenameVisitor : IEntryVisitor
{
    private readonly string _name;
    
    public RenameVisitor(string name)
    {
        _name = name;
    }
    
    public void Visit(FileModel file)
    {
        string path = Path.GetDirectoryName(file.Path) + "\\" + _name;
        File.Move(file.Path, path);
    }

    public void Visit(DirectoryModel directory)
    {
        string path = Path.GetDirectoryName(directory.Path) + "\\" + _name;
        Directory.Move(directory.Path, path);
    }
}