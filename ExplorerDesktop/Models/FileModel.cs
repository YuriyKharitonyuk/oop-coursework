﻿using System;
using System.IO;

namespace ExplorerDesktop;

[Serializable]
internal sealed class FileModel : EntryModel
{
    public FileModel(FileSystemInfo fileSystemInfo) 
        : base(fileSystemInfo)
    {
    }

    public override void Accept(IEntryVisitor visitor)
    {
        visitor.Visit(this);
    }
}