﻿using System.IO;
using System.Threading.Tasks;

namespace ExplorerDesktop;

internal sealed class MoveVisitor : IEntryVisitor
{
    private readonly string _path;
    
    public MoveVisitor(string path)
    {
        _path = path;
    }
    
    public async void Visit(FileModel file)
    {
        try
        {
            await MoveFileAsync(file.Path, _path);
        }
        catch
        {
            // ignored
        }
    }

    public async void Visit(DirectoryModel directory)
    {
        try
        {
            await MoveDirectoryAsync(directory.Path, _path);
            Directory.Delete(directory.Path, true);
        }
        catch
        {
            // ignored
        }
    }
    
    private async Task MoveFileAsync(string target, string path)
    {
        var sourceStream = File.Open(target, FileMode.Open);
        string destination = Path.Combine(path, Path.GetFileName(target));
        var destinationStream = File.Create(destination);
        await sourceStream.CopyToAsync(destinationStream);
        sourceStream.Close();
        File.Delete(target);
    }

    private async Task MoveDirectoryAsync(string target, string destination)
    {
        var combined = Path.Combine(destination, Path.GetFileName(target));
        Directory.CreateDirectory(combined);
        
        var targetInfo = new DirectoryInfo(target);

        foreach (var file in targetInfo.GetFiles())
        {
            await MoveFileAsync(file.FullName, combined);
        }

        foreach (var directory in targetInfo.GetDirectories())
        {
            await MoveDirectoryAsync(directory.FullName, combined);
        }
    }
}