﻿using System;
using System.IO;

namespace ExplorerDesktop;

[Serializable]
internal sealed class DirectoryModel : EntryModel
{
    public DirectoryModel(FileSystemInfo fileSystemInfo) 
        : base(fileSystemInfo)
    {
    }

    public override void Accept(IEntryVisitor visitor)
    {
        visitor.Visit(this);
    }
}