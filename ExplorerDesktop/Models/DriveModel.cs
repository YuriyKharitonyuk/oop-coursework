﻿using System.IO;

namespace ExplorerDesktop;

internal sealed class DriveModel
{
    public DriveInfo Info { get; }

    public long UsedSpace => Info.TotalSize - Info.AvailableFreeSpace;
    
    public DriveModel(DriveInfo driveInfo)
    {
        Info = driveInfo;
    }
}