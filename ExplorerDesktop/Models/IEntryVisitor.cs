﻿namespace ExplorerDesktop;

internal interface IEntryVisitor
{
    void Visit(FileModel file);
    
    void Visit(DirectoryModel directory);
}