﻿namespace ExplorerDesktop;

internal sealed class LanguageModel
{
    public string Name { get; }
    
    public string Code { get; }

    public LanguageModel(string name, string code)
    {
        Name = name;
        Code = code;
    }
}