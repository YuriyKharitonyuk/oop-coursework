﻿using System;
using System.IO;

namespace ExplorerDesktop;

[Serializable]
internal abstract class EntryModel
{
    public string Name { get; }

    public string Path { get; }

    public DateTime CreationTime { get; }

    public DateTime AccessTime { get; }

    public DateTime WriteTime { get; }

    public bool IsHidden { get; }

    protected EntryModel(FileSystemInfo fileSystemInfo)
    {
        Name = fileSystemInfo.Name;
        Path = fileSystemInfo.FullName;
        CreationTime = fileSystemInfo.CreationTime;
        AccessTime = fileSystemInfo.LastAccessTime;
        WriteTime = fileSystemInfo.LastWriteTime;
        IsHidden = fileSystemInfo.Attributes.HasFlag(FileAttributes.Hidden);
    }

    public abstract void Accept(IEntryVisitor visitor);
}