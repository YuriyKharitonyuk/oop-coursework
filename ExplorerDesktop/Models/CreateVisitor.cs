﻿using System;
using System.IO;

namespace ExplorerDesktop;

internal sealed class CreateVisitor : IEntryVisitor
{
    public void Visit(FileModel file)
    {
        if (File.Exists(file.Path))
        {
            throw new Exception($"File with name \"{file.Name}\" already exist here.");
        }

        File.Create(file.Path).Close();
    }

    public void Visit(DirectoryModel directory)
    {
        if (Directory.Exists(directory.Path))
        {
            throw new Exception($"Directory with name \"{directory.Name}\" already exist here.");
        }

        Directory.CreateDirectory(directory.Path);
    }
}