﻿using System.IO;
using System.Threading.Tasks;

namespace ExplorerDesktop;

internal sealed class CopyVisitor : IEntryVisitor
{
    private readonly string _path;
    
    public CopyVisitor(string path)
    {
        _path = path;
    }

    public async void Visit(FileModel file)
    {
        try
        {
            await CopyFileAsync(file.Path, _path);
        }
        catch
        {
            // ignored
        }
    }

    public async void Visit(DirectoryModel directory)
    {
        try
        {
            await CopyDirectoryAsync(directory.Path, _path);
        }
        catch
        {
            // ignored
        }
    }

    private async Task CopyFileAsync(string target, string path)
    {
        await using var sourceStream = File.Open(target, FileMode.Open);
        
        string destination = Path.Combine(path, Path.GetFileName(target));
        await using var destinationStream = File.Create(destination);
        
        await sourceStream.CopyToAsync(destinationStream);
    }

    private async Task CopyDirectoryAsync(string target, string destination)
    {
        var combined = Path.Combine(destination, Path.GetFileName(target));
        Directory.CreateDirectory(combined);

        var targetInfo = new DirectoryInfo(target);

        foreach (var file in targetInfo.GetFiles())
        {
            await CopyFileAsync(file.FullName, combined);
        }

        foreach (var directory in targetInfo.GetDirectories())
        {
            await CopyDirectoryAsync(directory.FullName, combined);
        }
    }
}