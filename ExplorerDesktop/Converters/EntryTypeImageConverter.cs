﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace ExplorerDesktop;

public class EntryTypeImageConverter : IMultiValueConverter
{
    private readonly Dictionary<string, string> _extensionResources = new()
    {
        { ".txt", "TextFileImage" },
        { ".rar", "ArchiveFileImage" },
        { ".zip", "ArchiveFileImage" },
        { ".gzip", "ArchiveFileImage" },
        { ".tar", "ArchiveFileImage" },
        { ".exe", "ExecutableFileImage" },
        { ".mp3", "MusicFileImage" }
    };

    private BitmapImage GetFileBitmapImage(string path, int decodeWidth)
    {
        try
        {
            string extension = Path.GetExtension(path).ToLower();

            if (extension == ".png" || extension == ".jpg" || extension == ".gif" || extension == ".bmp")
            {
                var source = new BitmapImage();

                source.BeginInit();

                source.CacheOption = BitmapCacheOption.OnLoad;
                source.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                source.DecodePixelWidth = decodeWidth;
                source.UriSource = new Uri(path);

                source.EndInit();
                source.Freeze();
                
                return source;
            }

            return Application.Current.GetResource<BitmapImage>(_extensionResources[extension]);
        }
        catch
        {
            return Application.Current.GetResource<BitmapImage>("UnknownFileImage");
        }
    }

    private BitmapImage GetDirectoryBitmapImage(string path)
    {
        Environment.SpecialFolder directoryType = default;
        
        foreach (Environment.SpecialFolder type in Enum.GetValues(typeof(Environment.SpecialFolder)))
        {
            if (path == Environment.GetFolderPath(type))
            {
                directoryType = type;
                break;
            }
        }

        string resourceName = directoryType switch
        {
            Environment.SpecialFolder.MyVideos => "VideosFolderImage",
            Environment.SpecialFolder.MyPictures => "PicturesFolderImage",
            Environment.SpecialFolder.MyMusic => "MusicFolderImage",
            Environment.SpecialFolder.Windows => "SystemFolderImage",
            _ => "DefaultFolderImage"
        };

        return Application.Current.GetResource<BitmapImage>(resourceName);
    }

    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
        if (values[0] is DirectoryModel directory)
        {
            return GetDirectoryBitmapImage(directory.Path);
        }

        if (values[0] is FileModel file)
        {
            double decodeWidth = (double)values[1];

            return GetFileBitmapImage(file.Path, (int)decodeWidth);
        }

        return DependencyProperty.UnsetValue;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}