﻿using System.Windows;

namespace ExplorerDesktop;

public partial class App
{
    protected override void OnStartup(StartupEventArgs e)
    {
        base.OnStartup(e);

        ApplicationSettings.Apply();
        Container.Resolve<MainWindow>().Show();
    }
}
