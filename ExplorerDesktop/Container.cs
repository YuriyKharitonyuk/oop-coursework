﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace ExplorerDesktop;

internal static class Container
{
    private static readonly IServiceProvider Provider;

    public static TService Resolve<TService>() where TService : notnull
    {
        return Provider.GetRequiredService<TService>();
    }
    
    static Container()
    {
        var services = new ServiceCollection();

        services.AddSingleton<IEntryStorage, EntryStorage>();
        services.AddSingleton<INavigationService, NavigationService>();
        services.AddSingleton<IHistoryNavigationService, HistoryNavigationService>();
        services.AddSingleton<IClipboardService, ClipboardService>();
        services.AddTransient<HomeViewModel>();
        services.AddTransient<CreateFileViewModel>();
        services.AddTransient<CreateDirectoryViewModel>();
        services.AddTransient<FilePropertiesViewModel>();
        services.AddTransient<DirectoryPropertiesViewModel>();
        services.AddTransient<SettingsViewModel>();
        services.AddTransient<EntriesViewModel>();
        services.AddSingleton<MainViewModel>();
        services.AddSingleton(provider => new MainWindow
        {
            DataContext = provider.GetRequiredService<MainViewModel>()
        });

        Provider = services.BuildServiceProvider();
    }
}