﻿using System.Linq;
using System.Windows.Controls;

namespace ExplorerDesktop;

public partial class EntriesView
{
    public EntriesView()
    {
        InitializeComponent();
    }

    private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (DataContext == null)
        {
            return;
        }
        
        var viewModel = (EntriesViewModel)DataContext;

        viewModel.SelectedEntries = EntriesListView.SelectedItems
            .Cast<EntryModel>()
            .ToList();
    }
}