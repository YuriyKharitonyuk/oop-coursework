﻿using System;
using System.Windows;

namespace ExplorerDesktop
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            StateChanged += OnStateChanged;
        }

        private void OnClose(object sender, RoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }
        
        private void OnMinimize(object sender, RoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }
        
        private void OnMaximize(object sender, RoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }
        
        private void OnRestore(object sender, RoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }
        
        private void OnStateChanged(object? sender, EventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                Root.BorderThickness = new Thickness(8);
                RestoreButton.Visibility = Visibility.Visible;
                MaximizeButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                Root.BorderThickness = new Thickness(0);
                RestoreButton.Visibility = Visibility.Collapsed;
                MaximizeButton.Visibility = Visibility.Visible;
            }
        }
    }
}