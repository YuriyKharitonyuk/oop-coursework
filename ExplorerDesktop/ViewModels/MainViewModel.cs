﻿namespace ExplorerDesktop;

internal sealed class MainViewModel : ViewModelBase
{
    private readonly INavigationService _navigationService;

    public ViewModelBase CurrentViewModel => _navigationService.Current;
        
    public MainViewModel(INavigationService navigationService)
    {
        _navigationService = navigationService;

        _navigationService.Changed += () => OnPropertyChanged(nameof(CurrentViewModel));
        _navigationService.Navigate<HomeViewModel>();
    }
}