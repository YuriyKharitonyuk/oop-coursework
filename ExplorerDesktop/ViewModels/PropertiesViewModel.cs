﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace ExplorerDesktop;

internal abstract class PropertiesViewModel : ViewModelBase
{
    private readonly INavigationService _navigationService;

    public string Name { get; set; }

    public bool IsHidden { get; set; }

    public EntryModel Entry { get; }

    public ICommand CancelCommand { get; }

    public ICommand ApplyCommand { get; }

    protected PropertiesViewModel(
        INavigationService navigationService,
        EntryModel entry)
    {
        _navigationService = navigationService;

        Name = entry.Name;
        IsHidden = entry.IsHidden;
        Entry = entry;

        CancelCommand = new NavigateCommand<EntriesViewModel>(navigationService);
        ApplyCommand = new RelayCommand(OnApply);
    }

    private void OnApply(object? obj)
    {
        try
        {
            if (Entry.Name != Name)
            {
                Entry.Accept(new RenameVisitor(Name));
            }
            
            if (IsHidden != Entry.IsHidden)
            {
                Entry.Accept(new ToggleAttributeVisitor(FileAttributes.Hidden));
            }

            _navigationService.Navigate<EntriesViewModel>();
        }
        catch (Exception exception)
        {
            MessageBox.Show(exception.Message, "Error");
        }
    }
}

internal class FilePropertiesViewModel : PropertiesViewModel
{
    public FilePropertiesViewModel(
        INavigationService navigationService, 
        EntryModel file) 
        : base(navigationService, file)
    {
    }
}

internal class DirectoryPropertiesViewModel : PropertiesViewModel
{
    public DirectoryPropertiesViewModel(
        INavigationService navigationService,  
        EntryModel directory) 
        : base(navigationService, directory)
    {
    }
}