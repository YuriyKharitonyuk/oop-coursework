﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace ExplorerDesktop;

internal sealed class HomeViewModel : ViewModelBase
{
    private readonly IEntryStorage _entryStorage;
    private readonly INavigationService _navigationService;
    private readonly IHistoryNavigationService _historyNavigationService;
    private CancellationTokenSource? _loadingCancellationTokenSource;

    public ObservableCollection<DriveModel> Drives { get; } = new();
    
    public ICommand OpenCommand { get; }
    
    public ICommand SettingsCommand { get; }
    
    public ICommand RefreshCommand { get; }

    public HomeViewModel(
        IEntryStorage entryStorage, 
        INavigationService navigationService, 
        IHistoryNavigationService historyNavigationService)
    {
        _entryStorage = entryStorage;
        _navigationService = navigationService;
        _historyNavigationService = historyNavigationService;

        OpenCommand = new RelayCommand<DriveModel?>(OnOpen);
        SettingsCommand = new NavigateCommand<SettingsViewModel>(navigationService);
        RefreshCommand = new RelayCommand(OnRefresh);

        Update();
    }

    private void Update()
    {
        try
        {
            Drives.Clear();
            _loadingCancellationTokenSource = new CancellationTokenSource();

            foreach (var entry in _entryStorage.GetDrives())
            {
                Application.Current.Dispatcher.Invoke(() => Drives.Add(entry), DispatcherPriority.Background, 
                    _loadingCancellationTokenSource.Token);
            }
        }
        catch
        {
            // ignored
        }
    }
    private void OnOpen(DriveModel? parameter)
    {
        _loadingCancellationTokenSource?.Cancel();
        
        try
        {
            if (parameter == null)
            {
                return;
            }


            _historyNavigationService.Add(new DirectoryModel(new DirectoryInfo(parameter.Info.Name)));
            _navigationService.Navigate<EntriesViewModel>();
        }
        catch (Exception exception)
        {
            MessageBox.Show(exception.Message, "Error");
        }
    }
    
    
    private void OnRefresh(object? obj)
    {
        Update();
    }
}