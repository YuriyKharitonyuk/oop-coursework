﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using GongSolutions.Wpf.DragDrop;

namespace ExplorerDesktop;

internal sealed class EntriesViewModel : ViewModelBase, IDropTarget
{
    private readonly INavigationService _navigationService;
    private readonly IHistoryNavigationService _historyNavigationService;
    private readonly IClipboardService _clipboardService;
    private readonly IEntryStorage _entryStorage;
    private readonly FileSystemWatcher _fileSystemWatcher;
    private CancellationTokenSource? _loadingCancellationTokenSource;
    private List<EntryModel> _selectedEntries = new();

    public ObservableCollection<EntryModel> Entries { get; } = new();

    public List<EntryModel> SelectedEntries
    {
        get => _selectedEntries;
        set
        {
            _selectedEntries = value;
            
            DeleteCommand.RaiseCanExecuteChanged();
            CopyCommand.RaiseCanExecuteChanged();
        }
    }

    public bool CanPaste => _clipboardService.IsFilled;

    public DirectoryModel Current => _historyNavigationService.Current;

    public ICommand OpenCommand { get; }

    public CommandBase DeleteCommand { get; }

    public CommandBase CopyCommand { get; }

    public ICommand PasteCommand { get; }
    
    public ICommand RefreshCommand { get; }

    public ICommand HomeCommand { get; }
    
    public ICommand CreateDirectoryCommand { get; }
    
    public ICommand CreateFileCommand { get; }
    
    public ICommand ShowPropertiesCommand { get; }

    public CommandBase BackCommand { get; }

    public CommandBase ForwardCommand { get; }

    public ICommand OpenBySystem { get; }

    public EntriesViewModel(
        INavigationService navigationService,
        IHistoryNavigationService historyNavigationService,
        IClipboardService clipboardService,
        IEntryStorage entryStorage)
    {
        _navigationService = navigationService;
        _historyNavigationService = historyNavigationService;
        _clipboardService = clipboardService;
        _entryStorage = entryStorage;
        _fileSystemWatcher = new FileSystemWatcher
        {
            Path = _historyNavigationService.Current.Path,
            EnableRaisingEvents = true
        };

        _fileSystemWatcher.Created += OnFileSystemWatcherCreate;
        _fileSystemWatcher.Renamed += OnFileSystemWatcherRename;
        _fileSystemWatcher.Deleted += OnFileSystemWatcherDelete;

        OpenCommand = new RelayCommand(OnOpen);
        DeleteCommand = new RelayCommand(OnDelete, _ => _selectedEntries.Count != 0);
        CopyCommand = new RelayCommand(OnCopy, _ => _selectedEntries.Count != 0);
        PasteCommand = new RelayCommand(OnPaste);
        RefreshCommand = new RelayCommand(OnRefresh);
        ShowPropertiesCommand = new RelayCommand(OnShowProperties);
        CreateDirectoryCommand = new NavigateCommand<CreateDirectoryViewModel>(navigationService);
        CreateFileCommand = new NavigateCommand<CreateFileViewModel>(navigationService);
        HomeCommand = new NavigateCommand<HomeViewModel>(navigationService);
        BackCommand = new RelayCommand(OnMoveBack, _ => historyNavigationService.CanMoveBack);
        ForwardCommand = new RelayCommand(OnMoveForward, _ => historyNavigationService.CanMoveForward);
        OpenBySystem = new RelayCommand(OnOpenBySystem);

        Update();
    }

    private void OnFileSystemWatcherDelete(object sender, FileSystemEventArgs e)
    {
        Application.Current.Start(() =>
        {
            for (int i = 0; i < Entries.Count; i++)
            {
                if (Entries[i].Path == e.FullPath)
                {
                    Entries.RemoveAt(i);
                    break;
                }
            }
        });
    }

    private void OnFileSystemWatcherRename(object sender, RenamedEventArgs e)
    {
        var attributes = File.GetAttributes(e.FullPath);

        Application.Current.Start(() =>
        {
            for (int i = 0; i < Entries.Count; i++)
            {
                if (Entries[i].Path == e.OldFullPath)
                {
                    if (attributes.HasFlag(FileAttributes.Directory))
                    {
                        Entries[i] = new DirectoryModel(new DirectoryInfo(e.FullPath));
                    }
                    else
                    {
                        Entries[i] = new FileModel(new FileInfo(e.FullPath));
                    }

                    break;
                }
            }
        });
    }

    private void OnFileSystemWatcherCreate(object sender, FileSystemEventArgs e)
    {
        var attributes = File.GetAttributes(e.FullPath);

        Application.Current.Start(() =>
        {
            if (attributes.HasFlag(FileAttributes.Directory))
            {
                Entries.Add(new DirectoryModel(new DirectoryInfo(e.FullPath)));
            }
            else
            {
                Entries.Add(new FileModel(new FileInfo(e.FullPath)));
            }
        });
    }

    private void Update()
    {
        try
        {
            OnPropertyChanged(nameof(Current));
            Entries.Clear();

            _fileSystemWatcher.Path = _historyNavigationService.Current.Path;
            _loadingCancellationTokenSource = new CancellationTokenSource();

            foreach (var entry in _entryStorage.GetContentOf(_historyNavigationService.Current.Path))
            {
                Application.Current.Start(() => Entries.Add(entry), _loadingCancellationTokenSource.Token);
            }
        }
        catch
        {
            // ignored
        }
    }

    private void OnOpen(object? parameter)
    {
        _loadingCancellationTokenSource?.Cancel();
        
        if (_selectedEntries[0] is FileModel file)
        {
            new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = file.Path,
                    UseShellExecute = true
                }
            }.Start();
        }
        else if (_selectedEntries[0] is DirectoryModel directory)
        {
            _historyNavigationService.Add(directory);
            ForwardCommand.RaiseCanExecuteChanged();
            BackCommand.RaiseCanExecuteChanged();
            OnPropertyChanged(nameof(Current));
            Update();
        }
    }

    private void OnDelete(object? parameter)
    {
        try
        {
            foreach (var entry in SelectedEntries)
            {
                entry.Accept(new DeleteVisitor());
                Entries.Remove(entry);
            }
        }
        catch (Exception exception)
        {
            MessageBox.Show(exception.Message, "Error");
        }
    }
    
    private void OnCopy(object? parameter)
    {
        try
        {
            _clipboardService.Set(SelectedEntries);
            OnPropertyChanged(nameof(CanPaste));
        }
        catch (Exception exception)
        {
            MessageBox.Show(exception.Message, "Error");
        }
    }

    private void OnPaste(object? parameter)
    {
        try
        {
            foreach (var entry in _clipboardService.Get())
            {
                entry.Accept(new CopyVisitor(_historyNavigationService.Current.Path));
            }
        }
        catch (Exception exception)
        {
            MessageBox.Show(exception.Message, "Error");
        }
    }
    
    private void OnRefresh(object? parameter)
    {
        _loadingCancellationTokenSource?.Cancel();
        Update();
    }

    private void OnShowProperties(object? parameter)
    {
        _loadingCancellationTokenSource?.Cancel();

        if (_selectedEntries[0] is DirectoryModel directory)
        {
            _navigationService.Navigate(new DirectoryPropertiesViewModel(_navigationService, directory));
        }
        else if (_selectedEntries[0] is FileModel file)
        {
            _navigationService.Navigate(new FilePropertiesViewModel(_navigationService, file));
        }
    }
    
    private void OnMoveBack(object? parameter)
    {
        _loadingCancellationTokenSource?.Cancel();
        _historyNavigationService.MoveBack();
        BackCommand.RaiseCanExecuteChanged();
        ForwardCommand.RaiseCanExecuteChanged();
        Update();
    }
    
    private void OnMoveForward(object? parameter)
    {
        _loadingCancellationTokenSource?.Cancel();
        _historyNavigationService.MoveForward();
        BackCommand.RaiseCanExecuteChanged();
        ForwardCommand.RaiseCanExecuteChanged();
        Update();
    }

    private void OnOpenBySystem(object? parameter)
    {
        try
        {
            Process.Start("explorer.exe", _historyNavigationService.Current.Path);
        }
        catch (Exception exception)
        {
            MessageBox.Show(exception.Message, "Error");
        }
    }


    public void DragOver(IDropInfo dropInfo)
    {
        if (dropInfo.TargetItem is DirectoryModel target)
        {
            if (dropInfo.Data is DirectoryModel directory)
            {
                if (target.Path == directory.Path)
                {
                    return;
                }
            }

            if (dropInfo.Data is IList<object> entryCollection)
            {
                foreach (var entry in entryCollection.Cast<EntryModel>())
                {
                    if (target.Path == entry.Path)
                    {
                        return;
                    }
                }
            }

            dropInfo.Effects = DragDropEffects.Move;
        }
    }

    public void Drop(IDropInfo dropInfo)
    {
        if (dropInfo.TargetItem is DirectoryModel target)
        {
            if (dropInfo.Data is EntryModel entryModel)
            {
                if (target.Path == entryModel.Path)
                {
                    return;
                }

                entryModel.Accept(new MoveVisitor(target.Path));
            }

            if (dropInfo.Data is IList<object> entryCollection)
            {
                foreach (var entry in entryCollection.Cast<EntryModel>())
                {
                    entry.Accept(new MoveVisitor(target.Path));
                }
            }
        }
    }
}