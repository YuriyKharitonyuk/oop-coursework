﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace ExplorerDesktop;

internal abstract class CreateViewModel<TEntry> : ViewModelBase
    where TEntry : EntryModel
{
    private readonly INavigationService _navigationService;
    private readonly IHistoryNavigationService _historyNavigationService;
    private readonly Func<string, TEntry> _instance;
    
    public ICommand CancelCommand { get; }

    public ICommand CreateCommand { get; }

    public CreateViewModel(
        INavigationService navigationService,
        IHistoryNavigationService historyNavigationService, 
        Func<string, TEntry> instance)
    {
        _navigationService = navigationService;
        _historyNavigationService = historyNavigationService;
        _instance = instance;

        CancelCommand = new NavigateCommand<EntriesViewModel>(navigationService);
        CreateCommand = new RelayCommand(OnCreate);
    }

    private void OnCreate(object? parameter)
    {
        try
        {
            string name = (string)parameter!;
            var path = Path.Combine(_historyNavigationService.Current.Path, name);

            _instance(path).Accept(new CreateVisitor());
            _navigationService.Navigate<EntriesViewModel>();
        }
        catch (Exception exception)
        {
            MessageBox.Show(exception.Message, "Error");
        }
    }
}

internal sealed class CreateFileViewModel : CreateViewModel<FileModel>
{
    public CreateFileViewModel( 
        INavigationService navigationService, 
        IHistoryNavigationService historyNavigationService) 
        : base(navigationService, historyNavigationService, path => new FileModel(new FileInfo(path)))
    {
    }
}

internal sealed class CreateDirectoryViewModel : CreateViewModel<DirectoryModel>
{
    public CreateDirectoryViewModel(
        INavigationService navigationService, 
        IHistoryNavigationService historyNavigationService) 
        : base(navigationService, historyNavigationService, path => new DirectoryModel(new DirectoryInfo(path)))
    {
    }
}