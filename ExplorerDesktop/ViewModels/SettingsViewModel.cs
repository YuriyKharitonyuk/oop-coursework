﻿using System.Collections.Generic;
using System.Windows.Input;

namespace ExplorerDesktop;

internal sealed class SettingsViewModel : ViewModelBase
{
    private readonly INavigationService _navigationService;
    
    public IEnumerable<LanguageModel> Languages { get; } = new[]
    {
        new LanguageModel("English", "en"),
        new LanguageModel("Українська", "uk"),
        new LanguageModel("Français", "fr-fr")
    };

    public string SelectedLanguage { get; set; } = ApplicationSettings.Language;
    
    public ICommand ApplyCommand { get; }

    public ICommand BackCommand { get; }
    
    
    public SettingsViewModel(INavigationService navigationService)
    {
        _navigationService = navigationService;
        
        ApplyCommand = new RelayCommand(OnApply);
        BackCommand = new NavigateCommand<HomeViewModel>(navigationService);
    }

    private void OnApply(object? parameter)
    {
        ApplicationSettings.Language = SelectedLanguage;
        
        ApplicationSettings.Apply();
        ApplicationSettings.Save();
        _navigationService.Navigate<HomeViewModel>();
    }
}