﻿using System;

namespace ExplorerDesktop;

public class RelayCommand : RelayCommand<object?>
{
    public RelayCommand(Action<object?> execute, Func<object?, bool>? canExecute = null) 
        : base(execute, canExecute)
    {
    }
}

public class RelayCommand<T> : CommandBase
{
    private readonly Action<T> _execute;
    private readonly Func<T, bool>? _canExecute;

    public RelayCommand(Action<T> execute, Func<T, bool>? canExecute = null)
    {
        _execute = execute;
        _canExecute = canExecute;
    }
    
    public override void Execute(object? parameter)
    {
        _execute((T)parameter);
    }

    public override bool CanExecute(object? parameter)
    {
        return _canExecute == null || _canExecute((T)parameter);
    }
}