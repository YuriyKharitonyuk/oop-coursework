﻿namespace ExplorerDesktop;

internal class NavigateCommand<TViewModel> : CommandBase
    where TViewModel : ViewModelBase
{
    private readonly INavigationService _viewNavigationService;

    public NavigateCommand(INavigationService viewNavigationService)
    {
        _viewNavigationService = viewNavigationService;
    }
    
    public override void Execute(object? parameter)
    {
        _viewNavigationService.Navigate<TViewModel>();
    }
}