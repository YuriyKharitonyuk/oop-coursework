﻿using System.Collections.Generic;
using System.IO;

namespace ExplorerDesktop;

internal sealed class EntryStorage : IEntryStorage
{
    public IEnumerable<DriveModel> GetDrives()
    {
        foreach (var drive in DriveInfo.GetDrives())
        {
            yield return new DriveModel(drive);
        }
    }

    public IEnumerable<EntryModel> GetContentOf(string path)
    {
        var directoryInfo = new DirectoryInfo(path);
        
        foreach (var directory in directoryInfo.EnumerateDirectories())
        {
            if ((directory.Attributes & FileAttributes.ReparsePoint) == 0)
            {
                yield return new DirectoryModel(directory);
            }
        }

        foreach (var file in directoryInfo.EnumerateFiles())
        {
            yield return new FileModel(file);
        }
    }
}