﻿using System;

namespace ExplorerDesktop;

internal interface INavigationService
{
    event Action? Changed;
    
    ViewModelBase Current { get; }

    T Navigate<T>() where T : ViewModelBase;
    
    void Navigate<T>(T instance) where T : ViewModelBase;
}