﻿using System;

namespace ExplorerDesktop;

internal sealed class NavigationService : INavigationService
{
    private ViewModelBase _current;

    public event Action? Changed;
    
    public ViewModelBase Current
    {
        get => _current;
        set
        {
            _current = value;
            Changed?.Invoke();
        }
    }

    public T Navigate<T>() where T : ViewModelBase
    {
        var viewModel = Container.Resolve<T>();
        Current = viewModel;
        return viewModel;
    }

    public void Navigate<T>(T instance) where T : ViewModelBase
    {
        Current = instance;
    }
}