﻿using System.Collections.Generic;

namespace ExplorerDesktop;

internal sealed class HistoryNavigationService : IHistoryNavigationService
{
    private readonly Stack<DirectoryModel> _previous = new();
    private readonly Stack<DirectoryModel> _next = new();
    
    public DirectoryModel? Current { get; private set; }

    public bool CanMoveBack => _previous.Count != 0;

    public bool CanMoveForward => _next.Count != 0;

    public void Add(DirectoryModel directory)
    {
        if (Current != null && Current.Path != directory.Path)
        {
            if (_next.Count != 0)
            {
                if (_next.Peek().Path == directory.Path)
                {
                    _next.Pop();
                }
            }

            _previous.Push(Current);
        }

        Current = directory;
    }

    public void MoveBack()
    {
        _next.Push(Current);
        Current = _previous.Pop();
    }
    
    public void MoveForward()
    {
        _previous.Push(Current);
        Current = _next.Pop();
    }
}