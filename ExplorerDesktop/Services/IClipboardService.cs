﻿using System.Collections.Generic;

namespace ExplorerDesktop;

internal interface IClipboardService
{
    bool IsFilled { get; }
    
    void Set(IEnumerable<EntryModel> entries);

    IEnumerable<EntryModel> Get();
}