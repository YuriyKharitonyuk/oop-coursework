﻿using System.Collections.Generic;
using System.Windows;

namespace ExplorerDesktop;

internal sealed class ClipboardService : IClipboardService
{
    private const string Key = "ExplorerClipboard";

    public bool IsFilled => Clipboard.ContainsData(Key);
    
    public void Set(IEnumerable<EntryModel> entry)
    {
        Clipboard.SetData(Key, entry);
    }

    public IEnumerable<EntryModel> Get()
    {
        return (IEnumerable<EntryModel>)Clipboard.GetData(Key);
    }
}