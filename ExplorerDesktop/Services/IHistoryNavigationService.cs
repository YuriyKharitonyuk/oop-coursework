﻿namespace ExplorerDesktop;

internal interface IHistoryNavigationService
{
    DirectoryModel Current { get; }
    
    bool CanMoveBack { get; }

    bool CanMoveForward { get; }

    void Add(DirectoryModel directory);

    void MoveBack();

    void MoveForward();
}