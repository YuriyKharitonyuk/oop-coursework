﻿using System.Collections.Generic;

namespace ExplorerDesktop;

internal interface IEntryStorage
{
    IEnumerable<DriveModel> GetDrives();

    IEnumerable<EntryModel> GetContentOf(string path);
}