﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace ExplorerDesktop;

public static class ApplicationExtension
{
    public static void Start(this Application application, Action action)
    {
        application.Dispatcher.Invoke(action, DispatcherPriority.Background);
    }
    
    public static void Start(this Application application, Action action, CancellationToken cancellationToken)
    {
        application.Dispatcher.Invoke(action, DispatcherPriority.Background, cancellationToken);
    }
    
    public static T GetResource<T>(this Application application, string name)
    {
        return (T)application.TryFindResource(name);
    }
}